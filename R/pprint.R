suppressPackageStartupMessages(library(xtable))

trim <- function (x) gsub("^\\s+|\\s+$", "", as.character(x))
ilevel <- function(x) sapply(strsplit(x, split=":", fixed=TRUE), length)

pprint <- function(model, ...) UseMethod("pprint")